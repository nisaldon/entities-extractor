const fs = require("fs");
const util = require("util");
const entitiesFolder = "./data/entities";
const readdir = util.promisify(fs.readdir);
// const readFile = utile.promisify(fs.readFile)

// async function readEntityFile(directory) {
//   fs.read
// }

async function readEntityFile(entityFile) {
  return new Promise((res, rej) => {
    fs.readFile(entitiesFolder + "/" + entityFile, "utf-8", (err, content) => {
      if (err) {
        return rej(err);
      }
      return res(JSON.parse(content));
    });
  });
}

function formatEntities(entities) {
  let entityData = {};
  entities.forEach(entity => {
    const dataToAdd = { [entity.value]: entity.synonyms };
    entityData = { ...entityData, ...dataToAdd };
  });
  return entityData
}

async function main() {
  const files = await readdir(entitiesFolder);
  const enititiesFiles = files.filter(file => file.includes("_en"));
  const promiseArray = enititiesFiles.map(entityFile => {
    return readEntityFile(entityFile);
  });
  const entities = await Promise.all(promiseArray);
  const formattedEntities = entities.reduce((acc, entities) => {
    const entityData = formatEntities(entities)
    return { ...acc, ...entityData };
  }, {});
  console.log(formattedEntities);
  return formattedEntities;
}

main();
